var graph = function(game) {

    this.game = game;

    this.y = null;
    this.ToaDoX = [];
    this.ToaDoY = [];
    this.DuyetDinh = [];

    // Sử dụng

    var x = 0;
    var xMap = 200;
    var yMap = 30;
    var ChieuRong2Dinh = 70;
    var ChieuCao2Dinh = 45;

    //BellMan
    this.Final = 0;
    this.mincost = null;
    this.previous = null;

    this.MangVet = [];

    function matrix(rows, cols, defaultValue) {

        var arr = [];

        // Creates all lines:
        for (var i = 0; i < rows; i++) {

            // Creates an empty line
            arr.push([]);

            // Adds cols to the empty line:
            arr[i].push(new Array(cols));

            for (var j = 0; j < cols; j++) {
                // Initializes:
                arr[i][j] = x;
            }
        }

        return arr;
    }

    this.init = function(DinhDau,DinhCuoi) {
        
        //Khoi tao ma tran mac dinh = 0;
        this.y = matrix(100, 100, 0);
        this.mincost = matrix(101,100,0);
        this.previous = matrix(101,100,0);

        CapNhatTrongSo(this.y);

        // for (var i = 0; i < 100; i++) {
        //     for (var j = 0; j < 100; j++) {
        //     	if(i != j && this.y[i][j] == 1){
        //     		console.log("i: "+ i+ " j: " + j + " " + this.y[i][j]);
        //     	}
        //     }
        // }
        //BellMan(this.y,DinhDau,DinhCuoi,this.mincost,this.previous);
        //Init
        var step = 0;
        for(var i = 0 ; i < 100 ; i++){
            this.mincost[step][i] = 32767;
            this.previous[step][i] = i;
        }
        this.mincost[step][DinhDau] = 0;
        //Bellman
        var bSuccess = false;
        var FinalStep = -1;
        for(var step = 1; step < 100; step++){
            for(var i = 0; i < 100; i++){//xet i
                this.mincost[step][i] = this.mincost[step-1][i];
                this.previous[step][i] = this.previous[step-1][i];
                for(var j = 0 ; j <100; j++){//duyet j->i
                    if(j!=i && this.mincost[step-1][j] != 32767 && this.y[j][i] != 0){//Kiem j
                        if(this.mincost[step-1][i] == 32767){ // Xet i hien tai co gia tri bang vo cuc
                            this.mincost[step][i] = this.mincost[step-1][j] + this.y[j][i];
                            this.previous[step][i] = j;
                        }
                        if(this.mincost[step-1][i] != 32767){
                            if(this.mincost[step-1][j] + this.y[j][i] < this.mincost[step-1][i]){
                                this.mincost[step][i] = this.mincost[step-1][j] + this.y[j][i];
                                this.previous[step][i] = j;
                            }
                        }
                    }
                }
            }
            var bSame = true;
            for(var i = 0; i < 100; i++){
                if(this.mincost[step][i] != this.mincost[step-1][i]){
                    bSame = false;
                    break;
                }
            }
            if(bSame == true){
                this.Final = step;
                break;
            }
        }
        var DuyetDinh = DinhCuoi;
        while(DuyetDinh != DinhDau){
            console.log(DuyetDinh+" <---- ");
            this.MangVet[DuyetDinh] = this.previous[this.Final][DuyetDinh];
            DuyetDinh = this.previous[this.Final][DuyetDinh];
            if (DuyetDinh == DinhDau){
                console.log(DuyetDinh);
            }
        }
    }

    function InitBellman (DinhDau, mincost, previous) {
        var step = 0;
        for(var i = 0 ; i < 100 ; i++){
            mincost[step][i] = 32767;
            previous[step][i] = i;
        }
        mincost[step][DinhDau] = 0;
    }

    function BellMan(MaTranTrongSo , DinhDau , DinhCuoi, mincost, previous){
        InitBellman(DinhDau,mincost,previous);
        var bSuccess = false;
        var FinalStep = -1;
        for(var step = 1; step < 100; step++){
            for(var i = 0; i < 100; i++){//xet i
                mincost[step][i] = mincost[step-1][i];
                previous[step][i] = previous[step-1][i];
                for(var j = 0 ; j <100; j++){//duyet j->i
                    if(j!=i && mincost[step-1][j] != 32767 && MaTranTrongSo[j][i] != 0){//Kiem j
                        if(mincost[step-1][i] == 32767){ // Xet i hien tai co gia tri bang vo cuc
                            mincost[step][i] = mincost[step-1][j] + MaTranTrongSo[j][i];
                            previous[step][i] = j;
                        }
                        if(mincost[step-1][i] != 32767){
                            if(mincost[step-1][j] + MaTranTrongSo[j][i] < mincost[step-1][i]){
                                mincost[step][i] = mincost[step-1][j] + MaTranTrongSo[j][i];
                                previous[step][i] = j;
                            }
                        }
                    }
                }
            }
            var bSame = true;
            for(var i = 0; i < 100; i++){
                if(mincost[step][i] != mincost[step-1][i]){
                    bSame = false;
                    break;
                }
            }
            if(bSame == true){
                this.Final = step;
                break;
            }
        }
        var DuyetDinh = DinhCuoi;
        while(DuyetDinh != DinhDau){
            console.log(DuyetDinh+" <---- ");
            DuyetDinh = previous[this.Final][DuyetDinh];
            if (DuyetDinh == DinhDau){
                console.log(DuyetDinh);
            }
        }
    }

    this.TraVeFinal = function(){
        return this.Final;
    }

    this.TraVemincost = function(){
        return this.mincost;
    }

    this.TraVeprevious = function(){
        return this.previous;
    }


    this.VeDinh = function()
    {
    	// Thiet lap toa do tung dinh
        for (var j = 0; j < 100; j++) {
            // Initializes:
            if (j < 10) {
                this.ToaDoX[j] = xMap;
                this.ToaDoY[j] = yMap;
                xMap = xMap + ChieuRong2Dinh;
            } else {
                if (j == 10 || j == 20 || j == 30 || j == 40 || j == 50 || j == 60 || j == 70 || j == 80 || j == 90) {
                    xMap = 200;
                    yMap = yMap + ChieuCao2Dinh;
                }
                this.ToaDoX[j] = xMap;
                this.ToaDoY[j] = yMap;
                xMap = xMap + ChieuRong2Dinh;
            }
        }

        for (var j = 0; j < 100; j++) {

            this.game.context.beginPath();

            this.game.context.fillStyle = "black";

            this.game.context.font = "20px Arial";
            this.game.context.arc(this.ToaDoX[j], this.ToaDoY[j], 10, 0, Math.PI * 2, true);
            this.game.context.fillText(j, this.ToaDoX[j], this.ToaDoY[j]);
        }
    }

    function CapNhatTrongSo(y) {
    	y[20][30] = 1;
    	y[30][20] = 1;

    	y[30][31] = 1;
    	y[31][30] = 1;

    	y[35][36] = 1;
    	y[36][35] = 1;

    	y[23][33] = 1;
    	y[33][23] = 1;

    	y[25][35] = 1;
    	y[35][25] = 1;

    	y[26][36] = 1;
    	y[36][26] = 1;

    	y[30][31] = 1;
    	y[31][30] = 1;

    	y[31][32] = 1;
    	y[32][31] = 1;

        y[32][33] = 1;y[33][32] = 1;

    	y[33][34] = 1;
    	y[34][33] = 1;

    	y[33][43] = 1;
    	y[43][33] = 1;

    	y[34][35] = 1;
    	y[35][34] = 1;

    	y[35][36] = 1;
    	y[36][35] = 1;

    	y[36][37] = 1;
    	y[37][36] = 1;

    	y[37][47] = 1;
    	y[47][37] = 1;

    	y[40][50] = 1;
    	y[50][40] = 1;

    	y[42][52] = 1;y[52][42] = 1;
    	y[43][53] = 1;y[53][43] = 1;
    	y[45][55] = 1;y[55][45] = 1;

    	y[47][58] = 1;y[58][47] = 1;
    	y[50][51] = 1;y[51][50] = 1;
    	y[51][52] = 1;y[52][51] = 1;
    	y[52][53] = 1;y[53][52] = 1;
    	y[53][54] = 1;y[54][53] = 1;
    	y[53][63] = 1;y[63][53] = 1;
    	y[54][55] = 1;y[55][54] = 1;
    	y[55][56] = 1;y[56][55] = 1;
    	y[58][59] = 1;y[59][58] = 1;

    	y[59][69] = 1;y[69][59] = 1;

    	y[56][67] = 1;y[67][56] = 1;

    	y[60][70] = 1;y[70][60] = 1;
    	y[63][73] = 1;y[73][63] = 1;
    	y[67][77] = 1;y[77][67] = 1;
    	y[69][79] = 1;y[79][69] = 1;

    	y[70][80] = 1;y[80][70] = 1;
        y[80][81] = 1;y[81][80] = 1;

    	y[71][81] = 1;y[81][71] = 1;
    	y[72][82] = 1;y[82][72] = 1;
    	y[73][83] = 1;y[83][73] = 1;
    	y[75][85] = 1;y[85][75] = 1;
    	y[77][86] = 1;y[86][77] = 1;
    	y[79][89] = 1;y[89][79] = 1;

    	y[80][81] = 1;y[81][80] = 1;
    	y[80][90] = 1;y[90][80] = 1;

    	y[81][82] = 1;y[82][81] = 1;
        y[82][83] = 1;y[83][82] = 1;
    	y[83][84] = 1;y[84][83] = 1;

    	y[84][85] = 1;y[85][84] = 1;
    	y[84][94] = 1;y[94][84] = 1;

    	y[85][86] = 1;y[86][85] = 1;
    	y[85][95] = 1;y[95][85] = 1;

    	y[88][89] = 1;y[89][88] = 1;
    	y[88][98] = 1;y[98][88] = 1;

    	y[94][95] = 1;y[95][94] = 1;

    	y[97][98] = 1;y[98][97] = 1;
	}

    this.TraVeToaDoDinhX = function(Dinh) {
        return this.ToaDoX[Dinh];
    }

    this.TraVeToaDoDinhY = function(Dinh) {
        return this.ToaDoY[Dinh];
    }
    this.taoMang = function(mang) {
        
        for(var i = 0; i < 100; i++){
            this.MangVet[i] = 0;
        }

        for(var i = 0 ; i < 100; i++){
            this.DuyetDinh[i] = 0; 
        }     
    }
}