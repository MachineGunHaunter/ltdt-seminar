var game = function() {
    this.width = 1000;
    this.height = 600;
    this.balls = [];
    this.canvas = null;
    this.context = null;
    this.blocks = [];
    this.newship = null;
    this.g = null;
    this.HienDinh = null;
    this.newMissle = null;

    this.i = 0;

    this.destinationX = 0;
    this.destinationY = 0;

    this.curMissileX = 0;
    this.curMissileY = 0;

    this.mang = [];

     //Khai bao bellman
    
    this.DinhDau = 80;
    this.DinhCuoi = 23;
    this.DuyetDinh = this.DinhCuoi;
    this.BuocCuoiCung = 0;
    this.snorlaxPOS = null;
    this.DenNoiDon = false;
    this.DuyetDinh2 = 0;
    this.MangVetNguoc =[];
    this.DonXong = false;

    var self = this;

    this.init = function() {
        this.canvas = document.getElementById("gameCanvas");
        this.context = this.canvas.getContext('2d');
        this.canvas.width = this.width;
        this.canvas.height = this.height;

        for (var i = 0; i < 100; i++) {
            var newBall = new ball(self);
            this.balls.push(newBall);
        }

        //
        //snorlax
        this.snorlaxPOS =  Math.floor((Math.random() * 100) + 1);
        console.log("snorlaxPOS: " + this.snorlaxPOS)
        while(this.snorlaxPOS == 21 || this.snorlaxPOS == 21 || this.snorlaxPOS == 24
            || this.snorlaxPOS == 27 || this.snorlaxPOS == 28 || this.snorlaxPOS == 29 || this.snorlaxPOS == 39
            || this.snorlaxPOS == 41 || this.snorlaxPOS == 42 || this.snorlaxPOS == 44 || this.snorlaxPOS == 46  || this.snorlaxPOS == 49
            || this.snorlaxPOS == 57 || this.snorlaxPOS == 59 || this.snorlaxPOS == 48
            || this.snorlaxPOS == 61 || this.snorlaxPOS == 62 || this.snorlaxPOS == 64 || this.snorlaxPOS == 65  || this.snorlaxPOS == 66
            || this.snorlaxPOS == 68 || this.snorlaxPOS == 38
            || this.snorlaxPOS == 71 || this.snorlaxPOS == 72 || this.snorlaxPOS == 74 || this.snorlaxPOS == 76 || this.snorlaxPOS == 78
            || this.snorlaxPOS == 87   || this.snorlaxPOS == 89 || this.snorlaxPOS == 91
            || this.snorlaxPOS == 92  || this.snorlaxPOS == 93  || this.snorlaxPOS == 96  || this.snorlaxPOS == 99
            || this.snorlaxPOS <= 29 || this.snorlaxPOS >99){

            this.snorlaxPOS =  Math.floor((Math.random() * 100) + 1);
        }
        this.DinhDau = this.snorlaxPOS;
        this.DuyetDinh2 = this.snorlaxPOS;

        //
        this.newship = new ship(self);
        
        this.g = new graph(self);
        this.g.taoMang(this.mang);
        this.g.VeDinh();
        this.g.init(this.DinhDau,this.DinhCuoi);
        
        this.destinationX = this.g.TraVeToaDoDinhX(69);
        this.destinationY = this.g.TraVeToaDoDinhY(69);

        console.log("Tra ve final: " +this.g.TraVeFinal());

        for(var i  = 0 ; i < 100; i++){

            this.MangVetNguoc[i] = 0;
        }

        for(var i  = 0 ; i < 100; i++){
            if(this.g.MangVet[i] != 0){
               console.log("i " + i +" :" +this.g.MangVet[i]);
               this.MangVetNguoc[this.g.MangVet[i]] = i;
            }
        }

        for(var i  = 0 ; i < 100; i++){
            if(this.MangVetNguoc[i] != 0){
               console.log("Vet Nguoc i " + i +" :" +this.MangVetNguoc[i]);
            }
            
        }
        this.newMissle = new missile(self,this.g.ToaDoX[23],this.g.ToaDoY[23]);

        this.update();
    }

    this.update = function() {
        self.updateAllBalls();
        self.updateShip();
        self.updateMissile();
        setTimeout(self.update, 20);
        self.draw();
    }

    this.draw = function() {
        self.drawBackground();
        self.drawAllBalls();
        self.drawShip();
        self.drawMissile();
    }

    this.updateAllBalls = function() {
        this.balls.forEach(function(ball) {
            ball.update();
        });
    }

    this.updateAllBlocks = function() {
        this.blocks.forEach(function(block) {
            block.update();
        });
    }

    this.updateShip = function() {
        var x = this.g.ToaDoX[this.snorlaxPOS];
        var y = this.g.ToaDoY[this.snorlaxPOS];
        this.newship.update(x,y,this.DenNoiDon);
    }

    this.updateMissile = function() {
        this.curMissileX = this.newMissle.curX();
        this.curMissileY = this.newMissle.curY();



        if (this.g.DuyetDinh[this.DuyetDinh] == 0 && this.DuyetDinh != 0 && this.DenNoiDon == false) {

            // console.log("ToaDoX cua dinh: " + this.g.TraVeToaDoDinhX(84));

            if (this.curMissileX < this.g.TraVeToaDoDinhX(this.DuyetDinh)) {
                this.newMissle.x++;
                this.newMissle.image.src = ".\\images\\car-right.png";
            }
            if (this.curMissileX > this.g.TraVeToaDoDinhX(this.DuyetDinh)) {
                this.newMissle.x--;
                this.newMissle.image.src = ".\\images\\car-left.png";
            }
            //console.log("Da Duyet " + this.g.DuyetDinh[this.i] + "ToaDoX TenLua: " + this.curMissileX + "ToaDoY TenLua: " + this.curMissileY + " " + "ToaDoDinhX " + this.g.TraVeToaDoDinhX(this.i) + "ToaDoDinhY " + this.g.TraVeToaDoDinhY(i) + " Dinh: " + i);

            if (this.curMissileY > this.g.TraVeToaDoDinhY(this.DuyetDinh)) {
                this.newMissle.y--;
                this.newMissle.image.src = ".\\images\\car-up.png";
            }

             if (this.curMissileY < this.g.TraVeToaDoDinhY(this.DuyetDinh)) {
                this.newMissle.y++;
                this.newMissle.image.src = ".\\images\\car-down.png";
            }
            //console.log("X: "+this.curMissileX + ": " + this.g.ToaDoX[this.DuyetDinh]);
            //console.log("Y: "+this.curMissileY + ": " + this.g.ToaDoY[23]);

            if (this.curMissileX == this.g.ToaDoX[this.DuyetDinh] && this.curMissileY == this.g.ToaDoY[this.DuyetDinh]) {
                this.g.DuyetDinh[this.DuyetDinh] = 1;
                this.DuyetDinh = this.g.MangVet[this.DuyetDinh];
                // this.DenNoiDon = true;
            }
            if (this.curMissileX == this.g.ToaDoX[this.snorlaxPOS] && this.curMissileY == this.g.ToaDoY[this.snorlaxPOS]) {
                this.DenNoiDon = true;   
            }
        }
        else if(this.DenNoiDon == true && this.DuyetDinh2 != 0){
            this.g.taoMang(this.mang);
            console.log("Duyet Dinh: " + this.DuyetDinh2);

            if (this.curMissileX < this.g.TraVeToaDoDinhX(this.DuyetDinh2)) {
                this.newMissle.x++;
                this.newMissle.image.src = ".\\images\\car-right.png";
            }
            if (this.curMissileX > this.g.TraVeToaDoDinhX(this.DuyetDinh2)) {
                this.newMissle.x--;
                this.newMissle.image.src = ".\\images\\car-left.png";
            }
            //console.log("Da Duyet " + this.g.DuyetDinh[this.i] + "ToaDoX TenLua: " + this.curMissileX + "ToaDoY TenLua: " + this.curMissileY + " " + "ToaDoDinhX " + this.g.TraVeToaDoDinhX(this.i) + "ToaDoDinhY " + this.g.TraVeToaDoDinhY(i) + " Dinh: " + i);

            if (this.curMissileY > this.g.TraVeToaDoDinhY(this.DuyetDinh2)) {
                this.newMissle.y--;
                this.newMissle.image.src = ".\\images\\car-up.png";
            }

             if (this.curMissileY < this.g.TraVeToaDoDinhY(this.DuyetDinh2)) {
                this.newMissle.y++;
                this.newMissle.image.src = ".\\images\\car-down.png";
            }
            //console.log("X: "+this.curMissileX + ": " + this.g.ToaDoX[this.DuyetDinh]);
            //console.log("Y: "+this.curMissileY + ": " + this.g.ToaDoY[23]);

            if (this.curMissileX == this.g.ToaDoX[this.DuyetDinh2] && this.curMissileY == this.g.ToaDoY[this.DuyetDinh2]) {
                this.g.DuyetDinh[this.DuyetDinh2] = 1;
                this.DuyetDinh2 = this.MangVetNguoc[this.DuyetDinh2];
                // this.DenNoiDon = true;
            }
            if (this.curMissileX == this.g.ToaDoX[23] && this.curMissileY == this.g.ToaDoY[23]) {
                this.DonXong = true;
                console.log("DonXong" + this.DonXong)
            }
        }
    }

    this.drawAllBalls = function() {
        this.balls.forEach(function(ball) {
            ball.draw(self);
        });
    }

    this.drawAllBlocks = function() {
        this.blocks.forEach(function(block) {
            block.draw();
        });
    }

    this.drawShip = function() {
        if(this.DenNoiDon == false){
            this.newship.draw(self);
        }    
    }

    this.drawMissile = function() {
        if(this.DonXong == false)
        {
            this.newMissle.draw(self);
        }
    }

    this.drawBackground = function() {
        // this.context.fillStyle = 'black';
        // this.context.fillRect(0,0,this.width,this.height);
        var img = new Image(15, 30);
        img.src = ".\\images\\city.jpg"
        this.context.drawImage(img, 10, 10);

        // this.HienDinh = new graph(self);
        // this.HienDinh.VeDinh();
    }
}

var ballGame = new game();
    ballGame.init();


function ChayTheoToaDo(){
    location.reload();
}