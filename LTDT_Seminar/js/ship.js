var ship = function(game) {
    this.game = game;

    this.x = 350;
    this.y = 480;

    this.image = new Image();
    this.image.src = ".\\images\\snorlax-down.png";

    this.speedX = Math.round(Math.random() * 5);
    this.speedY = Math.round(Math.random() * 5);

    this.update = function(x,y,DenDoiDon) {
    	if(DenDoiDon == false){
    		this.x = x;
        	this.y = y;
    	}
        // else{
        // 	this.x = 30;
        // 	this.y = 30;
        // }

        var chuoi = this.image.src;
        var n = chuoi.length;

        var cat = chuoi.substring(chuoi.length-16, chuoi.length);
        //console.log("chuoi cat: " + cat);

        if(cat == "snorlax-down.png"){
        	this.image.src = ".\\images\\snorlax-up.png";
        }
        else{
        	this.image.src = ".\\images\\snorlax-down.png";
        }

    }

    this.draw = function(game) {

        this.game.context.drawImage(this.image, this.x-50, this.y-50, this.image.width -50, this.image.height - 50);
    }
}